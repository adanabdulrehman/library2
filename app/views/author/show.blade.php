@extends('layouts.member.main')

	@section('header')
		AUTHOR - {{{$author->name}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
		{{{ isset($message) ? $message : '' }}}
		<br>Nationality: 
		<br>{{{$author->nationality}}}
		
	    <br><br>All books by {{{$author->name}}}<br><br>
	    <ul>
	    	@foreach($books as $abook)
	    		<li><a href="{{{URL::to('book')}}}/{{{$abook->id}}}">{{{$abook->title}}}</a></li>
		  	@endforeach
		</ul>

	@stop