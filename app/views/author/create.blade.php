@extends('layouts.member.main')

	@section('header')
		Add an Author 
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
		{{Form::model($author, array('route' => array('author.store'), 'method' => 'post'))}}
		    <h1> Please complete fields below to add an author 
		    <span>&nbsp;</span>
		    </h1>
		    
		    <label><span>Name :</span>{{Form::text('name')}}</label>
		    <label><span>Nationality :</span>{{Form::text('nationality')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Add')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	