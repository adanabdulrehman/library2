@extends('layouts.member.main')

	@section('header')
		Add a book 
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
		{{Form::model($book, array('route' => array('book.store'), 'method' => 'post'))}}
		    <h1> Please complete fields below to add a new book 
		    <span>&nbsp;</span>
		    </h1>
		    <label><span>Title :</span>{{Form::text('title')}}</label>
		    <label><span>Isbn :</span>{{Form::text('isbn')}}</label>
			<label><span>Published :</span>{{Form::text('publication_date')}} </label>
			<label><span>Author :</span>{{Form::select('author_id', $authors->lists('name', 'id'))}}</label>
			<label><span>Genre :</span>{{Form::select('genre_id', $genres->lists('name', 'id'))}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Add')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	