@extends('layouts.member.main')

	@section('header')
		BOOK - {{{$book->title}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
		<br>Book ID    : {{{$book->id}}}
		<br>Title    : {{{$book->title}}}
	    <br>Author   : <a href="{{{URL::to('author')}}}/{{{$author->id}}}">{{{$author->name}}}</a>
		<br>Isbn     : {{{$book->isbn}}}
		<br>Published: {{{$book->publication_date}}}
		<br>Category : <a href="{{{URL::to('genre')}}}/{{{$book->genre->id}}}">{{{$book->genre->name}}}</a>
		<br><br><a href="{{{URL::to('review')}}}/{{{$book->id}}}">Click this link to See reviews</a>
	    <br><br>Other books by {{{$author->name}}}<br>
	    
	    <ul>
	    @if(count($books))
	    	@foreach($books as $abook)
	    		@if($abook->title != $book->title)	
					<li><a href="{{{URL::to('book')}}}/{{{$abook->id}}}">Book ID: {{{$abook->id}}} - {{{$abook->title}}}</a></li>
				@endif
		  	@endforeach
		</ul>
		@else
			<br> There are no other books by this author
		@endif
	@stop