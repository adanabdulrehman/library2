@extends('layouts.member.main')

	@section('header')
		Edit {{{$book->title}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
		{{Form::model($book, array('route' => array('book.update', $book->id), 'method' => 'put'))}}
		    <h1> {{{$book->title}}}  
		    <span>{{Form::label('id', $book->id)}}</span>
		    <span>Author - {{$book->author->name}}</span>
		    <span>&nbsp;</span>
		    </h1>
		    
		    <label><span>Title :</span>{{Form::text('title')}}</label>
		    <label><span>Isbn :</span>{{Form::text('isbn')}}</label>
			<label><span>Published :</span>{{Form::text('publish_date')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Update')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	