@extends('layouts.member.main')
   @section('header')
       MEMBER SECTION
   @stop
 
   @section('leftMenu')
   @parent
   @stop
 
@section('content')
<section class="home_display">
<h7>Book yourself.</h7>
<p><br>Welcome to the city council book lending library home page.
	If you are already one of our <b>{{{$numUsers}}}</b> registered users,
	to access functionality please choose a menu item from the navigation links
	on the left hand side.
</p><br>
<p>Unregistered Visitors can browse the libraries resources or register to
become a full member with the link provided in the navigation.
Any problems should be directed to admin@leabharlann.com
Alternatively, by phone at 012 345 678 Ext 9</p>
<br>Thank you for continuing to support this service'
<br>-The Council Library Administration
<br>
   <div class="booklist">
    
               <br>{{{ isset($message) ? $message : '' }}}
               <table>
               <h8><b>Current Most Borrowed Books</b></b></h8>
                   <thead>
                           <td>Title</td>
                           <td>Author</td>
                           <td>Times Borrowed</td>
                           <td></td>
                       </tr>
                   </thead>
           
                    <tbody>
                   @foreach($userloans as $userloan)
                    <tr>
                           <td>{{{$userloan->book->title}}}</a></td>
                           <td>{{{$userloan->book->author->name}}}</a></td>
                           <td>{{{$userloan->loan_count}}}</td>
                       </tr>
                       @endforeach
        </tbody>
               </table>
               </div>
               <br>
         <div class="booklist">
    	
               <br>{{{ isset($message) ? $message : '' }}}
               <table>
               <h8><b>Highest Rated Books</b></h8>
                   <thead>
                           <td>Title</td>
                           <td>Author</td>
            				<td>Average score</td>
                           <td></td>
                       </tr>
                   </thead>
           
                    <tbody>
                    
                    @if(count($reviews))
                    number of books{{{count($reviews)}}}
                   		@foreach($reviews as $review)
                    	   <tr>
                           <td>{{{$review->book->title}}}</a></td>
                           <td>{{{$review->book->author->name}}}</a></td>
                           <td>{{{$review->averageScore}}}</a>
                            @if($review->averageScore < 1)
					    	{{ HTML::image('images/0star.jpg', $alt="Score Zero", $attributes = array()) }}
					    	@elseif ($review->averageScore >= 1 && $review->averageScore < 2)
							    {{ HTML::image('images/1star.jpg', $alt="Score One", $attributes = array()) }}
							    @elseif ($review->averageScore >= 2 && $review->averageScore < 3)
								    {{ HTML::image('images/2star.jpg', $alt="Score Two", $attributes = array()) }}
								    @elseif ($review->averageScore >= 3 && $review->averageScore < 4)
									    {{ HTML::image('images/3star.jpg', $alt="Score Three", $attributes = array()) }}
									    @elseif ($review->averageScore >= 4 && $review->averageScore < 5)
										    {{ HTML::image('images/4star.jpg', $alt="Score Four", $attributes = array()) }}
										    @elseif ($review->averageScore >= 5)
											    {{ HTML::image('images/5star.jpg', $alt="Score Five", $attributes = array()) }}
											   
					    @endif
                           </td>
                       	   </tr>
                       	@endforeach
                   @else
                   	No highest rated books
                   @endif
                   
        </tbody>
               </table>
               </div>          
               </section>
@stop


