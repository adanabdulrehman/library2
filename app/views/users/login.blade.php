@extends('layouts.login.main')
	@section('header')
		USER LOGIN
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
@section('content')

{{ Form::open(array('url'=>'signin', 'method' => 'post')) }}
    <h1>Please Login by filling all fields below</h1>
 
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
 	<span>&nbsp;</span>
	</h1>
    <label><span>Email address :</span>{{ Form::text('email') }}</label>
    <label><span>Password :</span>{{ Form::password('password') }}</label>
 
    <label><span>&nbsp;</span>{{ Form::submit('Login')}}</label>

{{ Form::close() }}
@stop
