<?php

// class used to seed the user table with data
// this is used for testing purposes
class UserSeeder extends Seeder {
	
	/**
	 * Run the user seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DB::table ( 'users' )->delete ();
		User::create ( array (
				'id' => '1',
				'username' => 'stephen',
				'firstname' => 'Stephen',
				'secondname' => 'Foley',
				'city' => 'sligo',
				'address' => '12 church lane',
				'phone' => '089735465',
				'outstanding_fine' => '0.00',
				'email' => 'stephen.foley@mycit.ie',
				'password' => Hash::make ( 'foley' ),
				'status' => 'complete',
				'type' => 'member',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '2',
				'username' => 'stephen',
				'firstname' => 'Stephen',
				'secondname' => 'Mckenna',
				'city' => 'galway',
				'address' => '45 Yew avenue',
				'phone' => '0873443222',
				'outstanding_fine' => '0.00',
				'email' => 'stephen.mckenna@mycit.ie',
				'password' => Hash::make ( 'mckenna' ),
				'status' => 'complete',
				'type' => 'librarian',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '3',
				'username' => 'ciara',
				'firstname' => 'Ciara',
				'secondname' => 'Foley',
				'city' => 'Cashel',
				'address' => '122 Willowbrook avenue',
				'phone' => '0903736445',
				'outstanding_fine' => '0.00',
				'email' => 'ciara.foley@mycit.ie',
				'password' => Hash::make ( 'foley' ),
				'status' => 'complete',
				'type' => 'Administrator',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '4',
				'username' => 'bartosz',
				'firstname' => 'Bartosz',
				'secondname' => 'Rojowski',
				'city' => 'waterford',
				'address' => '69 Maple drive',
				'phone' => '08745362',
				'outstanding_fine' => '0.00',
				'email' => 'bartosz.rojowski@mycit.ie',
				'password' => Hash::make ( 'rojowski' ),
				'status' => 'complete',
				'type' => 'member',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '5',
				'username' => 'Odette',
				'firstname' => 'Odette',
				'secondname' => 'Chavez',
				'city' => 'Patalillo',
				'address' => '4199 Sem.Av.',
				'phone' => '7956069232',
				'outstanding_fine' => '0',
				'email' => 'placerat.eget@urnaetarcu.org',
				'password' => Hash::make ( 'OdetteChavez' ),
				'status' => 'complete',
				'type' => 'member',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '6',
				'username' => 'jstamper',
				'firstname' => 'Jane',
				'secondname' => 'Stamper',
				'city' => 'sligo',
				'address' => '12 church lane',
				'phone' => '089735465',
				'outstanding_fine' => '0.00',
				'email' => 'member@library.ie',
				'password' => Hash::make ( 'password' ),
				'status' => 'complete',
				'type' => 'member',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '7',
				'username' => 'jstamper',
				'firstname' => 'Jane',
				'secondname' => 'Stamper',
				'city' => 'sligo',
				'address' => '12 church lane',
				'phone' => '089735465',
				'outstanding_fine' => '0.00',
				'email' => 'librarian@library.ie',
				'password' => Hash::make ( 'password' ),
				'status' => 'complete',
				'type' => 'librarian',
				'book_allowance' => '4' 
		) );
		
		User::create ( array (
				'id' => '8',
				'username' => 'jstamper',
				'firstname' => 'Jane',
				'secondname' => 'Stamper',
				'city' => 'sligo',
				'address' => '12 church lane',
				'phone' => '089735465',
				'outstanding_fine' => '0.00',
				'email' => 'administrator@library.ie',
				'password' => Hash::make ( 'password' ),
				'status' => 'complete',
				'type' => 'administrator',
				'book_allowance' => '4' 
		) );
	}
}