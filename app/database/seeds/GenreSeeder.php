<?php

// class used to seed the genres table with data
// this is used for testing purposes
class GenreSeeder extends Seeder {
	
	/**
	 * Run the genre seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DB::table ( 'genres' )->delete ();
		
		Genre::create ( array (
				'id' => '1',
				'name' => 'Early History',
				'description' => '12th - 16th Century history',
		) );
		
		Genre::create ( array (
				'id' => '2',
				'name' => 'Science fiction',
				'description' => 'Fantasy science and made up stuff about Aliens and outer space crap',
		) );
		
		Genre::create ( array (
				'id' => '3',
				'name' => 'Religion & Spirituality',
				'description' => 'Books on the search for meaning in life events and a yearning for connectedness to the universe.',
		) );
		
		Genre::create ( array (
				'id' => '4',
				'name' => 'Fashion',
				'description' => 'Books on popular style or practice, especially in clothing, footwear, accessories, makeup, body piercing, or furniture.',
		) );
		
		Genre::create ( array (
				'id' => '5',
				'name' => 'Modern Cuisine',
				'description' => 'Books on the Art and Science of Cooking in the 21st Century',
		) );
	}
}