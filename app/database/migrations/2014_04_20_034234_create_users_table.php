<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// author adam abdulrehman
class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Creates the users table
        Schema::create('users', function($table)
        { 
            $table->increments('id');
            $table->string('username', 50);
            $table->string('firstname', 50);
            $table->string('secondname', 50);
            $table->string('city', 50);
            $table->string('address', 255);
            $table->string('phone', 50);
            $table->double('outstanding_fine')->default(0.00);
            $table->string('email', 255)->unique();
            $table->string('password', 60);
            $table->string('status')->nullable()->default('pending');
            $table->string('type')->nullable()->default('member');
            $table->integer('book_allowance')->nullable()->default(4);
            $table->string('remember_token', 255)->nullable();
            $table->string('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
        });

        // Creates password reminders table
        Schema::create('password_reminders', function($t)
        {
            $t->string('email');
            $t->string('token');
            $t->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('password_reminders');
        Schema::drop('users');
    }

}
