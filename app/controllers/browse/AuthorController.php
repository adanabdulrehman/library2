<?php

class AuthorController extends \BaseController {

	// add a filter for authorization only allowing index and show resource
	function __construct() {
		$this->beforeFilter ( 'administrator', array ('except' => array ('index', 'show')) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
	
	// Display a listing of the resource.
	// @return Response
	public function index() {
		$authors = Author::all ();
		return View::make ( 'author.index' )->with ( 'authors', $authors );
	}
	
	// Show the form for creating a new resource.
    // @return Response
	public function create() {
		$author = new Author;
		return View::make ('author.create')->with ( 'author', $author );
	}
	
	// Store a newly created resource in storage.
    // @return Response
	public function store() {
	
		$author=new Author;
	
		$inputs = Input::all ();
		$author->name = $inputs ['name'];
		$author->nationality = $inputs ['nationality'];
		$author->save();
		return Redirect::route ('author.index');
	}
	
	// Display the specified resource.
	// @param int $id
	// @return Response 
	public function show($id) {
		$author = Author::find ( $id );
	    $books = $author->books;
		$message = Session::get('message', '');
	
		return View::make ('author.show')->with ('author', $author)->with ('books', $books)->with('message', $message);
	}
	
	// Show the form for editing the specified resource.
    // @param int $id
    // @return Response
	public function edit($id) {
		$author = Author::find ( $id );
		return View::make ( 'author.edit' )->with ( 'author', $author );
	}
	
	// Update the specified resource in storage.
	// @param int $id
	// @return Response
	public function update($id) {
	
		$author = Author::find ( $id );
		$inputs = Input::all ();
		$author->name = $inputs ['name'];
		$author->nationality = $inputs ['nationality'];
		$author->update ();
	
		return Redirect::route ('author.show', array($id))->with ('message', 'Author updated.');
	}
	
	// Remove the specified resource from storage.
    // @param int $id
	// @return Response
	public function destroy($id) {
		$author = Author::find($id);
		$author->delete();
	}
}
