<?php

// This is the administrator controller
class AdminController extends BaseController {

	function __construct() {
	
		// authenticate all actions
		$this->beforeFilter ( 'administrator', array (
				'except' => array ()
		) );
	
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post'
		) );
	}
	
	// Default Home Controller
	public function home() {
		return View::make('administrator/home');
	}

}