<?php

// book model
class Book extends Eloquent {

	public function loans() {
		return $this->hasMany ('Loan');
	}
	
	public function reviews() {
		return $this->hasMany ('Review');
	}
	
	// A book will have an author
	public function author() {
		return $this->belongsTo ( 'Author' );
	}
	
	// A book will belong to a genre
	public function genre() {
		return $this->belongsTo ( 'Genre' );
	}
	
	// define a query scope for available books
	// to use this simply do:
    // $books = Book::available()->orderBy('created_at')->get();
	public function scopeAvailable($query)
	{
		return $query->where('available', '=', 1);
	}
	
	// define a query scope for non-available books
	public function scopeUnavailable($query)
	{
		return $query->where('available', '!=', 1);
	}
	
} 